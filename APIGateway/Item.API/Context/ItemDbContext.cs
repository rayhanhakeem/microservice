﻿using Item.API.Models;
using Microsoft.EntityFrameworkCore;

namespace Item.API.Context
{
	public class ItemDbContext : DbContext
	{
		public ItemDbContext(DbContextOptions<ItemDbContext> options) : base(options) { }

		/*protected override void OnModelCreating(ModelBuilder modelBuilder) { }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
		{
			base.OnConfiguring(optionsBuilder);
		}*/

		public DbSet<Items> Items { get; set; }
	}
}
