using Item.API.Context;
using Item.API.Models;
using Item.API.Pagination;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Security.Claims;

namespace Item.API.Controllers
{
	
	[Route("api/[controller]")]
	[ApiController]
	public class ItemController : ControllerBase
	{
		private readonly ItemDbContext _context;
		private readonly IUriService uriService;

		public ItemController(ItemDbContext context, IUriService uriService)
		{
			_context = context;
			this.uriService = uriService;
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Penjual, Pembeli")]
		[HttpGet("getStock-1")]
		public async Task<ActionResult> GetStock(int id, int qty)
		{
			var req = Request;
			var checkItem = await _context.Items
				.Where(Q => Q.ItemId == id)
				.FirstOrDefaultAsync();
			if (checkItem == null)
			{
				return BadRequest("Item not found!");
			}
			checkItem.Stock = checkItem.Stock - qty;
			await _context.SaveChangesAsync();
			return Ok();
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Penjual, Pembeli")]
		[HttpGet("getAllItem")]
		public async Task<List<Items>> GetAll()
		{
			if (User.FindFirstValue(ClaimTypes.Role) == "Pembeli")
			{
				var getList = new List<Items>();
				var list = await _context.Items.ToListAsync();
				foreach(var items in list)
				{
					var itemGetModel = new Items()
					{
						ItemId = items.ItemId,
						SellerId = items.SellerId,
						Name = items.Name,
						Price= items.Price,
						Stock = items.Stock,
					};
					getList.Add(itemGetModel);
				}
				return getList;
			} else
			{
				var currentUser = Convert.ToInt32(User.FindFirstValue(ClaimTypes.Name));
				var userExist = await _context.Items
					.Where(Q => Q.SellerId == currentUser)
					.ToListAsync();
				var getList = new List<Items>();
				foreach (var item in userExist)
				{
					var itemGetModel = new Items()
					{
						ItemId = item.ItemId,
						SellerId =item.SellerId,
						Name = item.Name,
						Price = item.Price,
						Stock = item.Stock,
					};
					getList.Add(itemGetModel);
				}
				return getList;
			}
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Penjual, Pembeli")]
		[HttpGet("getAllItem/{filter}")]
		public async Task<IActionResult> GetAllFilter([FromQuery] PaginationFilter filter)
		{
			var currentUser = Convert.ToInt32(User.FindFirstValue(ClaimTypes.Name));
			var route = Request.Path.Value;
			var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
			if(User.FindFirstValue(ClaimTypes.Role) == "Penjual")
			{
				var pagedData = await _context.Items
					.Where(Q=> Q.SellerId == currentUser)
					.Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
					.Take(validFilter.PageSize)
					.ToListAsync();
				var totalRecords = await _context.Items.Where(Q => Q.SellerId == currentUser).CountAsync();
				var pagedReponse = PaginationHelper.CreatePagedReponse<Items>(pagedData, validFilter, totalRecords, uriService, route);
				return Ok(pagedReponse);
			} else
			{
				var pagedData = await _context.Items
				.Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
				.Take(validFilter.PageSize)
				.ToListAsync();
				var totalRecords = await _context.Items.CountAsync();
				var pagedReponse = PaginationHelper.CreatePagedReponse<Items>(pagedData, validFilter, totalRecords, uriService, route);
				return Ok(pagedReponse);
			}
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Penjual, Pembeli")]
		[HttpGet("search")]
		public async Task<IActionResult> Search(string search)
		{
			var currentUser = Convert.ToInt32(User.FindFirstValue(ClaimTypes.Name));
			if (_context.Items == null)
			{
				return Problem("Entity set 'Items' is null.");
			}

			if(User.FindFirstValue(ClaimTypes.Role) == "Penjual")
			{
				var items = from m in _context.Items where m.SellerId == currentUser
							select m;

				if (!String.IsNullOrEmpty(search))
				{
					items = items.Where(s => s.Name!.Contains(search));
				}

				return Ok(await items.ToListAsync());
			}
			else
			{
				var items = from m in _context.Items
							select m;

				if (!String.IsNullOrEmpty(search))
				{
					items = items.Where(s => s.Name!.Contains(search));
				}

				return Ok(await items.ToListAsync());
			}
			
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Penjual, Pembeli")]
		[HttpGet("getItem")]
		public async Task<ActionResult> GetbyId([FromQuery] int id)
		{
			if(User.FindFirstValue(ClaimTypes.Role) == "Pembeli")
			{
				var itemExist = await _context.Items
				.Where(Q => Q.ItemId == id)
				.FirstOrDefaultAsync();

				if (itemExist == null)
				{
					return BadRequest("Item not found!");
				}

				return Ok(itemExist);
			} else
			{
				var currentUser = Convert.ToInt32(User.FindFirstValue(ClaimTypes.Name));
				var itemExist = await _context.Items
				.Where(Q => Q.ItemId == id && Q.SellerId == currentUser)
				.FirstOrDefaultAsync();
				if (itemExist == null)
				{
					return BadRequest("Item not found!");
				}
				return Ok(itemExist);
			}
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Penjual, Pembeli")]
		[HttpGet("getItemBySellerId")]
		public async Task<ActionResult> GetbySellerId([FromQuery] int id)
		{
			if (User.FindFirstValue(ClaimTypes.Role) == "Pembeli")
			{
				var itemExist = await _context.Items
				.Where(Q => Q.SellerId == id)
				.ToListAsync();

				if (itemExist == null)
				{
					return BadRequest("Item not found!");
				}

				var getList = new List<Items>();
				foreach (var item in itemExist)
				{
					var itemGetModel = new Items()
					{
						ItemId = item.ItemId,
						SellerId = item.SellerId,
						Name = item.Name,
						Price = item.Price,
						Stock = item.Stock,
					};
					getList.Add(itemGetModel);
				}

				return Ok(getList);
			}
			else
			{
				var currentUser = Convert.ToInt32(User.FindFirstValue(ClaimTypes.Name));
				var itemExist = await _context.Items
				.Where(Q => Q.SellerId == id && Q.SellerId == currentUser)
				.ToListAsync();
				if (itemExist == null)
				{
					return BadRequest("Item not found!");
				}
				var getList = new List<Items>();
				foreach (var item in itemExist)
				{
					var itemGetModel = new Items()
					{
						ItemId = item.ItemId,
						SellerId = item.SellerId,
						Name = item.Name,
						Price = item.Price,
						Stock = item.Stock,
					};
					getList.Add(itemGetModel);
				}

				return Ok(getList);
			}
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Penjual")]
		[HttpPost("createItem")]
		public async Task<ActionResult> CreateItem([FromBody] ItemCreate item)
		{
			var currentUser = User.FindFirstValue(ClaimTypes.Name);
			var createItem = new Items();
			createItem.SellerId = Convert.ToInt32(currentUser);
			createItem.Price = item.Price;
			createItem.Name = item.Name;
			createItem.Stock = item.Stock;


			_context.Items.Add(createItem);

			await _context.SaveChangesAsync();
			return Ok(new { Item = item });
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Penjual")]
		[HttpPut("updateItem")]
		public async Task<ActionResult> UpdateItem([FromQuery] int itemId, [FromBody] ItemCreate item)
		{
			var currentUser = Convert.ToInt32(User.FindFirstValue(ClaimTypes.Name));
			var itemExist = await _context.Items
				.Where(Q => Q.ItemId == itemId && Q.SellerId == currentUser)
				.FirstOrDefaultAsync();

			if (itemExist?.ItemId != itemId)
			{
				return BadRequest("Item not found!");
			}
			if (itemExist.SellerId != currentUser)
			{
				return BadRequest("Invalid Item ID!");
			}

			itemExist.Name = item.Name;
			itemExist.Price = item.Price;
			itemExist.Stock = item.Stock;

			_context.Items.Update(itemExist);
			await _context.SaveChangesAsync();
			return Ok(new { Updated = itemExist });
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Penjual")]
		[HttpDelete("deleteItem")]
		public async Task<ActionResult> DeleteItem(int id)
		{
			var currentUser = Convert.ToInt32(User.FindFirstValue(ClaimTypes.Name));
			var itemExist = await _context.Items
				.Where(Q => Q.ItemId == id && Q.SellerId == currentUser)
				.FirstOrDefaultAsync();
			if (itemExist?.ItemId != id)
			{
				return BadRequest("Item not found!");
			}
			if (itemExist.SellerId != currentUser)
			{
				return BadRequest("Invalid Item ID!");
			}
			_context.Items.Remove(itemExist);

			await _context.SaveChangesAsync();
			return Ok(new { Deleted = itemExist });
		}
	}
}