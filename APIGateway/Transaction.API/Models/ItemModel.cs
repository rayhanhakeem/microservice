﻿namespace Transaction.API.Models
{
    public class ItemModel
    {
        public int ItemId { get; set; }
		public int SellerId { get; set; }
		public string Name { get; set; }
		public decimal Price { get; set; }
		public int Stock { get; set; }
    }
}
