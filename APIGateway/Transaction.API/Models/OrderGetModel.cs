﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;

namespace Transaction.API.Models
{
	[Keyless]
	public class OrderGetModel
	{
		public int OrderId { get; set; }
		public int SellerId { get; set; }
		public int? ItemId { get; set; }
		public string? ItemName { get; set; } = string.Empty;
		public int Quantity { get; set; }
		public string? CustomerName { get; set; } = string.Empty;
		public string Status { get; set; } = string.Empty;
		public string Date { get; set; } = string.Empty;
	}
}
