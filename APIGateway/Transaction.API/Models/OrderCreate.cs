﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Transaction.API.Models
{
	public class OrderCreate
	{
		public int ItemId { get; set; }
		public int Quantity { get; set; }
	}
}
