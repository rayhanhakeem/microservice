﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Transaction.API.Models
{
	public class Orders
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int OrderId { get; set; }
		public int BuyerId { get; set; } 
		public int SellerId { get; set; }
		public int ItemId { get; set; } 
		public int Quantity { get; set; }
		public DateTime DateOrder { get; set; } 
		public string OrderStatus { get; set; }  
	}
}
