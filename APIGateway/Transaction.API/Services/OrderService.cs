﻿using Transaction.API.Context;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Transaction.API.Models;
using System.Security.Claims;
using RestSharp;
using RestSharp.Authenticators;

namespace Transaction.API.Services
{
	public class OrderService
	{
		private readonly OrderDbContext _context;
		private readonly IHttpContextAccessor _httpContextAccessor;

		public OrderService(OrderDbContext context, IHttpContextAccessor httpContextAccessor)
		{
			_context = context;
			_httpContextAccessor = httpContextAccessor;
		}

		public async Task<(ItemModel? item, string message)> CheckItem(int itemId, string token)
		{
			var baseUrl = "https://localhost:7219/item/api/Item/";
			var endPoint = "getItem";
			var client = new RestClient($"{baseUrl}{endPoint}");

			client.Authenticator = new JwtAuthenticator(token);

			var request = new RestRequest();

			request.AddQueryParameter("id", itemId);

			ItemModel? item;
			try
			{
				item = await client.GetAsync<ItemModel>(request);
			}
			catch (Exception e)
			{
				return (null, e.Message);
			}

			return (item, string.Empty);
		}

		public async Task<(UserModel? user, string message)> CheckUser(int userId, string token)
		{
			var baseUrl = "https://localhost:7219/user/api/Users/";
			var endPoint = "getUserById";
			var client = new RestClient($"{baseUrl}{endPoint}");

			client.Authenticator = new JwtAuthenticator(token);

			var request = new RestRequest();

			request.AddQueryParameter("id", userId);

			UserModel? user;
			try
			{
				user = await client.GetAsync<UserModel>(request);
			}
			catch (Exception e)
			{
				return (null, e.Message);
			}

			return (user, string.Empty);
		}

		public async Task<int?> CheckStock(int id, int qty, string token)
		{
			var baseUrl = "https://localhost:7219/item/api/Item/";
			var endPoint = "getStock-1";
			var client = new RestClient($"{baseUrl}{endPoint}");

			client.Authenticator = new JwtAuthenticator(token);

			var request = new RestRequest();
			request.AddQueryParameter("id", id);
			request.AddQueryParameter("qty", qty);

			int item;
			try
			{
				item = await client.GetAsync<int>(request);
			}
			catch (Exception e)
			{
				return null;
			}

			return item;
		}

		public async Task<string?> GetCurrentUserToken()
		{
			var baseUrl = "https://localhost:7219/";
			var endPoint = "user/api/Users/GetCurrentUserToken";
			var client = new RestClient($"{baseUrl}{endPoint}");

			var request = new RestRequest();

			string? token;
			try
			{
				token = await client.PostAsync<string>(request);
			}
			catch (Exception e)
			{
				return null;
			}

			return token;
		}

		public Orders Create(ItemModel model, int quantity)
		{
			int currentUserId = Convert.ToInt32(_httpContextAccessor?.HttpContext?.User.FindFirstValue(ClaimTypes.Name));

			var newOrder = new Orders
			{
				BuyerId = currentUserId,
				SellerId = model.SellerId,
				ItemId = model.ItemId,
				DateOrder = DateTime.Now,
				OrderStatus = "OK",
				Quantity= quantity
			};
			return newOrder;
		}
	}
}
