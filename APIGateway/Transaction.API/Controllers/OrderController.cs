﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data;
using Transaction.API.Context;
using Transaction.API.Models;
using System.Security.Claims;
using Transaction.API.Services;
using Microsoft.AspNetCore.Server.IIS.Core;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Transaction.API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class OrderController : ControllerBase
	{
		private readonly OrderDbContext _context;
		private readonly OrderService orderService;

		public OrderController(OrderDbContext context, OrderService _orderService)
		{
			_context = context;
			this.orderService = _orderService;
		}

		[HttpGet("getAllTransaction")] // GET ALL KELAR
		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Pembeli, Penjual")]
		public async Task<ActionResult<List<OrderGetModel>>> GetAll()
		{
			var getList = new List<OrderGetModel>();
			var order = new Orders();
			var userid = Convert.ToInt32(User.FindFirstValue(ClaimTypes.Name));

			var token = await this.orderService.GetCurrentUserToken();

			if (token == null)
			{
				return BadRequest("Cannot get user token");
			}

			if (User.FindFirstValue(ClaimTypes.Role) == "Penjual") {
				var currentOrder = await this._context.Orders
				.AsNoTracking()
				.Where(Q => Q.SellerId == userid)
				.ToListAsync(); // cari table item yang sesuai sama user id si penjual, lalu taro di list
				if (currentOrder == null)
				{
					return BadRequest("No Orders!");
				}
				// cari item list pake where

				foreach (var orders in currentOrder)
				{
					(var customerName, var messageUser) = await this.orderService.CheckUser(orders.BuyerId, token);

					 

					(var currentItem, var messageItem) = await this.orderService.CheckItem(orders.ItemId, token);
					var itemGetModel = new OrderGetModel()
					{
						OrderId = orders.OrderId,
						SellerId = orders.SellerId,
						CustomerName = customerName?.Name,
						Quantity = orders.Quantity,
						ItemName = currentItem?.Name,
						ItemId = orders.ItemId,
						Status = orders.OrderStatus,
						Date = orders.DateOrder.ToString()
					};
					getList.Add(itemGetModel);
				}
			}
			else
			{
				var currentOrder = await this._context.Orders
				.AsNoTracking()
				.Where(Q => Q.BuyerId == userid)
				.ToListAsync();
				if(currentOrder == null)
				{
					return BadRequest("No Transactions!");
				}
				foreach (var item in currentOrder)
				{
					(var currentItem, var messageItem) = await this.orderService.CheckItem(item.ItemId, token);

					var itemGetModel = new OrderGetModel()
					{
						OrderId = item.OrderId,
						SellerId = item.SellerId,
						CustomerName = User.FindFirstValue(ClaimTypes.NameIdentifier),
						Quantity = item.Quantity,
						ItemName = currentItem?.Name,
						ItemId = item.ItemId,
						Status = item.OrderStatus,
						Date = item.DateOrder.ToString()
					};
					getList.Add(itemGetModel);
				}
			}
			return getList;
		}

		[HttpGet("getTransactionById")] // KELAR!!
		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Pembeli, Penjual")]
		public async Task<ActionResult> GetbyId([FromQuery] int id)
		{
			var token = await this.orderService.GetCurrentUserToken();

			if (token == null)
			{
				return BadRequest("Cannot get user token");
			}

			var currentUser = Convert.ToInt32(User.FindFirstValue(ClaimTypes.Name));
			if (User.FindFirstValue(ClaimTypes.Role) == "Pembeli")
			{
				var orderExist = await _context.Orders
					.Where(Q => Q.OrderId == id && Q.BuyerId == currentUser)
					.FirstOrDefaultAsync();
				if (orderExist == null)
				{
					return BadRequest("Transaction not found!");
				}else
				{
					(var itemExist, var messageItem) = await this.orderService.CheckItem(orderExist.ItemId, token);

					if (itemExist == null)
					{
						return BadRequest("Item not found!");
					}

					return Ok(new OrderGetModel
					{
						OrderId = orderExist.OrderId,
						SellerId = itemExist.SellerId,
						CustomerName = User.FindFirstValue(ClaimTypes.NameIdentifier),
						Quantity = orderExist.Quantity,
						ItemName = itemExist.Name,
						ItemId = orderExist.ItemId,
						Status = orderExist.OrderStatus,
						Date = orderExist.DateOrder.ToString()
					});
				}
				
			}
			else
			{
				var itemExist = await _context.Orders
					.Where(Q => Q.SellerId == currentUser)
					.FirstOrDefaultAsync();
				if (itemExist == null)
				{
					return BadRequest("You have not items!");
				}else
				{
					var orderExist = await _context.Orders
					.Where(Q => Q.OrderId == id && Q.ItemId == itemExist.ItemId)
					.FirstOrDefaultAsync();

					if (orderExist == null)
					{
						return BadRequest("Transaction not found!");
					}else
					{
						(var customerName, var messageUser) = await this.orderService.CheckUser(orderExist.BuyerId, token);
						(var currentItem, var messageItem) = await this.orderService.CheckItem(orderExist.ItemId, token);
						return Ok(new OrderGetModel
						{
							OrderId = orderExist.OrderId,
							SellerId = itemExist.SellerId,
							CustomerName = customerName.Name,
							ItemName = currentItem.Name,
							ItemId = orderExist.ItemId,
							Status = orderExist.OrderStatus,
							Date = orderExist.DateOrder.ToString()
						});
					}
					
				}
			}
		}

		[HttpPost("createTransaction")] // KELAR
		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Pembeli")]
		public async Task<ActionResult> CreateTransaction([FromBody] List<OrderCreate> orders)
		{
			foreach (var order in orders)
			{

				var token = await this.orderService.GetCurrentUserToken();

				if (token == null)
				{
					return BadRequest("Cannot get user token");
				}

				var createOrder = new Orders();
				(var item, var message) = await this.orderService.CheckItem(order.ItemId, token);
				if (item == null)
				{
					if (string.IsNullOrEmpty(message) == false)
					{
						return BadRequest(message);
					}
					return BadRequest("Item cannot be found");
				}
				

				var newOrder = this.orderService.Create(item, order.Quantity);

				var newStock = this.orderService.CheckStock(item.ItemId, order.Quantity, token);

				_context.Orders.Add(newOrder);

				await _context.SaveChangesAsync();
			}
			return Ok(orders);

		}

		[HttpPut("updateTransaction")] // KELAR
		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Pembeli")]
		public async Task<ActionResult> UpdateTransaction([FromQuery] int orderId, [FromBody] OrderUpdate orderStatus)
		{
			var token = await this.orderService.GetCurrentUserToken();

			if (token == null)
			{
				return BadRequest("Cannot get user token");
			}

			var currentUser = Convert.ToInt32(User.FindFirstValue(ClaimTypes.Name));
			var orderExist = await _context.Orders
				.Where(Q => Q.OrderId == orderId && Q.BuyerId == currentUser)
				.FirstOrDefaultAsync();

			if (orderExist == null)
			{
				return BadRequest("Invalid Order ID!");
			}
			else
			{
				(var currentItem, var messageItem) = await this.orderService.CheckItem(orderExist.ItemId, token);
				if (orderStatus.OrderStatus != "OK")
				{
					orderExist.OrderStatus = "CANCEL";
					currentItem.Stock += orderExist.Quantity;
					_context.Orders.Update(orderExist);
					//_context.Items.Update(itemExist);
					await _context.SaveChangesAsync();
					return Ok(new { Updated = orderExist });
				}
				else
				{
					return Ok("You don't change any data!");
				}
			}
		}

		[HttpDelete("deleteTransaction")] // KELAR
		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Penjual")]
		public async Task<ActionResult> DeleteTransaction(int id)
		{
			var currentUser = User.FindFirstValue(ClaimTypes.Name);
			var itemExist = await _context.Orders
				.Where(Q => Q.OrderId == id && Q.BuyerId == Convert.ToInt32(currentUser))
				.FirstOrDefaultAsync();
			if (itemExist == null)
			{
				return BadRequest("Transaction not found!");
			}
			var cancel = "CANCEL";
			if(itemExist.OrderStatus == cancel)
			{
				_context.Orders.Remove(itemExist);
				await _context.SaveChangesAsync();
			} else
			{
				return BadRequest("The buyer has not updated the transaction to CANCEL the order!");
			}
			return Ok(new { Deleted = itemExist });
		}
	}
}
