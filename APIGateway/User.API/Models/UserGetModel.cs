﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace User.API.Models
{
	public class UserGetModel
	{
		public int UserId { get; set; }
		public string Name { get; set; } = string.Empty;
		public string Username { get; set; } = string.Empty;
		public string Role { get; set; } = string.Empty;
		public string Address { get; set; } = string.Empty;
		//[RegularExpression("^(\\+62|62|0)8[1-9][0-9]{6,9}$", ErrorMessage = "Invalid Phone Number")]
		public long PhoneNumber { get; set; }
	}
}
