﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace User.API.Models
{
	public class UserRegister
	{
		[Required]
		public string Name { get; set; } = string.Empty;
		[Required]
		public string Username { get; set; } = string.Empty;
		[Required]
		[DataType(DataType.Password)]
		public string Password { get; set; } = string.Empty;
		[Required]
		public string Role { get; set; } = string.Empty;
		[Required]
		public string Address { get; set; } = string.Empty;
		[Required]
		//[RegularExpression("^(\\+62|62|0)8[1-9][0-9]{6,9}$", ErrorMessage = "Invalid Phone Number")]
		public long PhoneNumber { get; set; }
	}
}
