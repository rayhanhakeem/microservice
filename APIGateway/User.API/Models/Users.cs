﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace User.API.Models
{
	public class Users
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int UserId { get; set; }
		[Required]
		public string Name { get; set; } = string.Empty;
		[Required]
		public string Username { get; set; } = string.Empty;
		[Required]
		[DataType(DataType.Password)]
		public string Password { get; set; } = string.Empty;
		[Required]
		public string Role { get; set; } = string.Empty;
		[Required]
		public string Address { get; set; } = string.Empty;
		[Required]
		//[RegularExpression("^(\\+62|62|0)8[1-9][0-9]{6,9}$", ErrorMessage = "Invalid Phone Number")]
		public long PhoneNumber { get; set; }
		public string Salt { get; set; } = string.Empty;
	
	}
}
