﻿using Microsoft.EntityFrameworkCore;
using User.API.Models;

namespace User.API.Context
{
	public class UserDbContext : DbContext
	{
		public UserDbContext(DbContextOptions<UserDbContext> options) : base(options) { }

		public DbSet<Users> Users { get; set; }
	}
}
