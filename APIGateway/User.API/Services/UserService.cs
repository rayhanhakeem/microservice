﻿using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using User.API.Context;
using User.API.Models;

namespace User.API.Services
{
	public class UserService
	{

		private readonly UserDbContext _context;

		public UserService(UserDbContext context)
		{
			this._context = context;
		}

		public async Task<Users?> GetCurrentUser(int userId)
		{
			var userData = await _context.Users.FirstOrDefaultAsync(x => x.UserId == Convert.ToInt32(userId));

			return userData;
		}
	}
}
