using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using User.API.Context;
using User.API.Models;
using User.API.Services;

namespace User.API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UsersController : ControllerBase
	{
		private readonly IOptions<MyConfiguration> ConfigurationOption;
		private readonly UserDbContext _context;
		private readonly UserService _userService;

		private static string CurrentUserJwtToken = string.Empty;

		public UsersController(UserDbContext context,
			IOptions<MyConfiguration> _configurationOption,
			UserService userService)
		{
			_context = context;
			ConfigurationOption = _configurationOption;
			this._userService = userService;
		}

		/*[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Penjual, Pembeli")]
		[HttpGet("getAllUser")]
		public async Task<List<UserRegister>> GetAll()
		{
			var currentUser = Convert.ToInt32(User.FindFirstValue(ClaimTypes.Name));
			if (User.FindFirstValue(ClaimTypes.Role) == "Penjual")
			{
				var users = await _context.Users.ToListAsync();
				List<UserRegister> list = new List<UserRegister>();
				foreach (Users user in users)
				{
					list.Add(new UserRegister { UserId = user.UserId, Name = user.Name, Username = user.Username, Password = user.Password, Role = user.Role, Address = user.Address, PhoneNumber = user.PhoneNumber });
				}
				return list;
			} else
			{
				var users = await _context.Users.Where(Q=>Q.UserId == currentUser).ToListAsync();
				List<UserRegister> list = new List<UserRegister>();
				foreach (Users user in users)
				{
					list.Add(new UserRegister { UserId = user.UserId, Name = user.Name, Username = user.Username, Password = user.Password, Role = user.Role, Address = user.Address, PhoneNumber = user.PhoneNumber });
				}
				return list;
			}
			
		}*/

		[AllowAnonymous]
		[HttpPost("GetCurrentUserToken")]
		public ActionResult<string> GetCurrentUserToken()
		{
			return Ok(CurrentUserJwtToken);
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Penjual, Pembeli")]
		[HttpGet("getUserById")]
		public async Task<ActionResult> GetbyId([FromQuery] int id)
		{
			//var currentUser = Convert.ToInt32(User.FindFirstValue(ClaimTypes.Name));
			var userExist = await _context.Users
				.AsNoTracking()
				.Where(Q => Q.UserId == id )//&& Q.UserId == currentUser)
				.FirstOrDefaultAsync();

			if (userExist == null)
			{
				return BadRequest("User not found!");
			}

			return Ok(new UserGetModel { UserId = userExist.UserId, Name = userExist.Name, Username = userExist.Username, Role = userExist.Role, Address = userExist.Address, PhoneNumber = userExist.PhoneNumber });
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Pembeli, Penjual")]
		[HttpGet("getCurrentUser")]
		public async Task<IActionResult> getCurrentUser()
		{
			var userId = Convert.ToInt32(User.FindFirstValue(ClaimTypes.Name));

			// Call service
			var user = await this._userService.GetCurrentUser(userId);

			if (user == null)
			{
				return BadRequest("User not found");
			}

			return Ok(new UserGetModel { UserId = user.UserId, Name = user.Name, Username = user.Username, Role = user.Role, Address = user.Address, PhoneNumber = user.PhoneNumber});

		}

		[AllowAnonymous]
		[HttpPost("createUser")]
		public async Task<ActionResult> CreateUser([FromBody] UserRegister userRegister)
		{
			if (ModelState.IsValid == false)
			{
				var error = ModelState.Select(x => x.Value.Errors)
						   .Where(y => y.Count > 0)
						   .ToList()
						   .ToString();
				return BadRequest(error);
			}

			var userExist = await _context.Users
				.Where(Q => Q.Username == userRegister.Username)
				.FirstOrDefaultAsync();

			if (userExist != null)
			{
				return BadRequest("Username already exist!");
			}

			if (userRegister.Role != "Penjual" && userRegister.Role != "Pembeli")
			{
				var test = "Role harus diisi dangan 'Penjual' atau 'Pembeli'";
				return BadRequest(test);
			}

			var salt = DateTime.Now.ToString();

			var HashedPW = HashPassword($"{userRegister.Password}{salt}");

			_context.Users.Add(new Users
			{
				Username = userRegister.Username,
				Name = userRegister.Name,
				PhoneNumber = userRegister.PhoneNumber,
				Address = userRegister.Address,
				Password = HashedPW,
				Role = userRegister.Role,
				Salt = salt
			});

			await _context.SaveChangesAsync();
			return Ok(new { DataUser = userRegister });
		}

		[HttpPost("loginUser")]
		public async Task<ActionResult<Users>> Login([FromBody] UserLogin user)
		{
			if (!string.IsNullOrEmpty(user.Username) &&
				!string.IsNullOrEmpty(user.Password))
			{
				var loggedInUser = await _context.Users
					.AsNoTracking()
					.Where(Q => Q.Username == user.Username)
					.FirstOrDefaultAsync();
				if (loggedInUser != null)
				{
					var password = loggedInUser.Password;
					var salt = loggedInUser.Salt;
					var HashedPW = HashPassword($"{user.Password}{salt}");

					if (password == HashedPW)
					{
						var claims = new[]
						{
							new Claim(ClaimTypes.NameIdentifier, loggedInUser.Username),
							new Claim(ClaimTypes.MobilePhone, loggedInUser.PhoneNumber.ToString()),
							new Claim(ClaimTypes.GivenName, loggedInUser.Name),
							new Claim(ClaimTypes.Role, loggedInUser.Role),
							new Claim(ClaimTypes.StreetAddress, loggedInUser.Address),
							new Claim(ClaimTypes.DateOfBirth, loggedInUser.Salt),
							new Claim(ClaimTypes.Name, loggedInUser.UserId.ToString())
						};

						var token = new JwtSecurityToken
						(
							issuer: this.ConfigurationOption.Value.Issuer,
							audience: this.ConfigurationOption.Value.Audience,
							claims: claims,
							expires: DateTime.UtcNow.AddDays(60),
							notBefore: DateTime.UtcNow,
							signingCredentials: new SigningCredentials(
								new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.ConfigurationOption.Value.Key)),
								SecurityAlgorithms.HmacSha256)
						);

						var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

						CurrentUserJwtToken = tokenString;

						return Ok(tokenString);
					}
					else
					{
						return NotFound("Username or Password is Wrong!");
					}
				}
			}
			return BadRequest("User tidak valid");
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Penjual")]
		[HttpPut("updateUser")]
		public async Task<ActionResult> UpdateUser([FromQuery] UserRegister user)
		{
			var currentUser = Convert.ToInt32(User.FindFirstValue(ClaimTypes.Name));
			var userExist = await _context.Users
				.Where(Q => Q.Username == user.Username && Q.UserId == currentUser)
				.FirstOrDefaultAsync();

			if (userExist?.Name != user.Name)
			{
				return BadRequest("Invalid Username!");
			}
			if (userExist.UserId != currentUser)
			{
				return BadRequest("Invalid User ID!");
			}
			if (userExist.Username == user.Username && userExist.Username != User.FindFirstValue(ClaimTypes.NameIdentifier))
			{
				return BadRequest("Username already exist");
			}

			var salt = DateTime.Now.ToString();
			var newHashedPW = HashPassword($"{user.Password}{salt}");

			userExist.Username = user.Username;
			userExist.Name = user.Name;
			userExist.Password = newHashedPW;
			userExist.Address = user.Address;
			userExist.PhoneNumber = user.PhoneNumber;
			userExist.Role = user.Role;
			userExist.Salt = salt;

			if (user.Role != "Penjual" && user.Role != "Pembeli")
			{
				var test = "Role harus diisi dangan 'Penjual' atau 'Pembeli'";
				return BadRequest(test);
			}

			_context.Users.Update(userExist);
			await _context.SaveChangesAsync();
			return Ok(new { Updated = userExist });
		}

		[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Penjual")]
		[HttpDelete("deleteUser")]
		public async Task<ActionResult> DeleteUser(int id)
		{
			var currentUser = Convert.ToInt32(User.FindFirstValue(ClaimTypes.Name));
			var userExist = await _context.Users
				.Where(Q => Q.UserId == id && Q.UserId == currentUser)
				.FirstOrDefaultAsync();
			if (userExist == null)
			{
				return BadRequest("Invalid Username!");
			}
			_context.Users.Remove(userExist);

			await _context.SaveChangesAsync();
			return Ok(new { Deleted = userExist });
		}

		private string HashPassword(string password)
		{
			SHA256 hash = SHA256.Create();

			var passwordBytes = Encoding.Default.GetBytes(password);

			var hashedpassword = hash.ComputeHash(passwordBytes);

			return Convert.ToHexString(hashedpassword);
		}
	}
}